# Ejercicios del curso HTML y CSS Básico
En este repositorio se encuentran los recursos relacionados y tareas del curso.
## Descripción
En este curso se abordan los conceptos básicos de HTML y CSS.
## Autor
* <López Lara Marco Antonio>
### Contacto
<marco.lopezl@ingenieria.unam.edu>
